FROM debian:buster-slim

RUN apt-get -y update && apt-get -y upgrade && apt-get -y install git curl gettext-base

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.17.3/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl ./usr/bin/
RUN curl -LO https://get.helm.sh/helm-v3.1.1-linux-amd64.tar.gz
RUN tar zfvx helm-v3.1.1-linux-amd64.tar.gz
RUN chmod +x ./linux-amd64/helm
RUN mv ./linux-amd64/helm /usr/bin/helm
